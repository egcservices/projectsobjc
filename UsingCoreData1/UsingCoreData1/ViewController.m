//
//  ViewController.m
//  UsingCoreData1
//
//  Created by Treinamento on 19/08/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import "ViewController.h"
#import "Product+CoreDataClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)save:(id)sender {
    
    NSDictionary *dic = @{@"name": self.txtName.text,
                          @"brand": self.txtBrand.text,
                          @"quantity": self.txtQuantity.text};
    
    [Product newProduct: dic];
    
    [self clearFields];
}

- (IBAction)clear:(id)sender {
    [self clearFields];
}

-(void)clearFields{
    self.txtName.text = @"";
    self.txtBrand.text = @"";
    self.txtQuantity.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
