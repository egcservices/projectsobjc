//
//  ListViewController.h
//  UsingCoreData1
//
//  Created by Treinamento on 19/08/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product+CoreDataClass.h"

@interface ListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSArray *arrayProducts;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Product *selectProduct;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@end
