//
//  Picture+CoreDataProperties.m
//  UsingCoreData1
//
//  Created by Treinamento on 19/08/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import "Picture+CoreDataProperties.h"

@implementation Picture (CoreDataProperties)

+ (NSFetchRequest<Picture *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Picture"];
}

@dynamic data;
@dynamic date;
@dynamic extension;
@dynamic product;

@end
