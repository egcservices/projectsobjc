//
//  Product+CoreDataProperties.m
//  UsingCoreData1
//
//  Created by Treinamento on 19/08/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Product"];
}

@dynamic name;
@dynamic quantity;
@dynamic brand;
@dynamic pictures;

@end
