//
//  LoginViewController.h
//  Aula1ObjC
//
//  Created by Treinamento on 29/07/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtNome;
@property (weak, nonatomic) IBOutlet UITextField *txtNumero;
@property (weak, nonatomic) IBOutlet UILabel *lblValueSlider;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIButton *btnAcionar;

@end
