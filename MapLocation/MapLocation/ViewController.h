//
//  ViewController.h
//  MapLocation
//
//  Created by Treinamento on 19/08/17.
//  Copyright © 2017 EGCservices. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) BOOL *firstLocation;
@property (strong, nonatomic) MKLocalSearch *localSearch;

@end

